package main

const (
	serverListKey               = "server-list"
	walletsDirKey               = "wallets-dir"
	httpPortKey                 = "PORT"
	httpHostKey                 = "HOST"
	rateLimitBucketSizeKey      = "RATE_LIMITTER_BUCKET_SIZE"
	rateLimitCleanupIntervalKey = "RATE_LIMITTER_CLEANUP_TIME"
	validatorWalletAddressKey   = "VALIDATOR_WALLET"
	lidBankWalletAddressKey     = "lidbank-wallet"
	lidRewardSenderKey          = "lid-reward-sender"
	lidExecutableKey            = "BUILD_EXEC"
	bootstrappingPeerKey        = "BOOTSTRAPPING_PEER"
	nodeTypeKey                 = "NODE_TYPE"
	priceDBKey                  = "pricedb"
)
