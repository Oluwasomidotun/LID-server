package database

import "errors"

// errors
var (
	ErrNotFound = errors.New("resource not found")
	ErrClosed   = errors.New("data store is closed")
)
