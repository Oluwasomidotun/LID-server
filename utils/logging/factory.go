package logging

import "path/filepath"

// Factory represents a Logger Factory Interface
type Factory interface {
	Build() (Logger, error)
	AddDir(prefix, dir string, skip bool) (Logger, error)
	Close()
}

type factory struct {
	config  Config
	loggers []Logger
}

// NewFactory returns a logger factory
func NewFactory(config Config) Factory {
	return &factory{
		config: config,
	}
}

// Build returns a new Logger
func (f *factory) Build() (Logger, error) {
	l, err := New(f.config)
	if err == nil {
		f.loggers = append(f.loggers, l)
	}
	return l, err
}

// AddDir ...
func (f *factory) AddDir(prefix, dir string, skip bool) (Logger, error) {
	config := f.config
	config.Prefix = prefix
	if !skip {
		config.Directory = filepath.Join(config.Directory, dir)
	}
	l, err := New(config)
	if err == nil {
		f.loggers = append(f.loggers, l)
	}
	return l, err
}

// Close ...
func (f *factory) Close() {
	f.loggers = nil
}
