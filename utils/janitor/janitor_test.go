package janitor

import (
	"os"
	"testing"
	"time"
)

var (
	jn *JN
)

func setUp(t *testing.T) {
	if _, err := os.Stat("test-data"); os.IsNotExist(err) {
		err = os.MkdirAll("test-data/testWallet", 0700)
		if err != nil {
			t.Errorf("failed to create test data dir: %v", err)
		}
	}

	jn = New(&Config{
		DispatchInterval: 10 * time.Second,
		CleanupInterval:  -1,
		DirPath:          "./test-data",
		MaxBlocksDel:     100,
	})
}

func tearDown(t *testing.T) {
	err := os.RemoveAll("test-data")
	if err != nil {
		t.Errorf("failed to remove test data dir: %v", err)
	}
}

func TestDispatchIsSucessful(t *testing.T) {
	setUp(t)
	defer tearDown(t)

	if jn.config == nil {
		t.Errorf("Janitor object not initialized, %v", jn)
	}
}

func TestCanClearOldBlocks(t *testing.T) {
	setUp(t)
	defer tearDown(t)

	f, err := os.OpenFile("test-data/testWallet/chain.bin", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		t.Error(err)
	}
	defer f.Close()

	jn.handleDispatch()
	time.Sleep(1 * time.Second)
	if _, err := os.Stat("test-data/testWallet"); !os.IsNotExist(err) {
		t.Error("handleDispatch failed to clear old block")
	}
}
